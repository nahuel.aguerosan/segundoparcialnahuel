<?php
// constante para el tamaño de la matriz
define("MATRIX_SIZE", 3); 

function generateMatrix() {
    $matrix = [];
    for ($i = 0; $i < MATRIX_SIZE; $i++) {
        for ($j = 0; $j < MATRIX_SIZE; $j++) {
          // Números aleatorios entre 0 y 9
            $matrix[$i][$j] = rand(0, 9); 
        }
    }
    return $matrix;
}

function printMatrix($matrix) {
    for ($i = 0; $i < MATRIX_SIZE; $i++) {
        for ($j = 0; $j < MATRIX_SIZE; $j++) {
            echo $matrix[$i][$j] . "\t";
        }
        echo "\n";
    }
}

function sumDiagonal($matrix) {
    $sum = 0;
    for ($i = 0; $i < MATRIX_SIZE; $i++) {
        $sum += $matrix[$i][$i];
    }
    return $sum;
}

do {
    $matrix = generateMatrix();
    echo "Matriz generada:\n";
    printMatrix($matrix);
    
    $sum = sumDiagonal($matrix);
    echo "Suma de la diagonal principal: $sum\n";

    if ($sum >= 10 && $sum <= 15) {
        echo "La suma de la diagonal principal está entre 10 y 15. Finalizando script.\n";
        break;
    } else {
        echo "Generando nueva matriz...\n";
    }
} while (true);

?>